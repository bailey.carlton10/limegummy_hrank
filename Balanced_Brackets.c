#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();

// Complete the isBalanced function below.

// Please either make the string static or allocate on the heap. For example,
// static char str[] = "hello world";
// return str;
//
// OR
//
// char* str = "hello world";
// return str;
//
char* isBalanced(char* s) {
    char oStr[4] = "({[";
    char cStr[4] = ")}]";
    int arrLen = strlen(s);
    char bracs[1001], *sp = &bracs[0]-1;
    char match = 0;
    int i = 0;
    int stackSize = 0;


    if(s[i] == cStr[0] || s[i] == cStr[1] || s[i] == cStr[2]){
        return "NO";
    }


    for (; i < arrLen; i++){

        // if the current bracket is an opener, push into stack
        if (s[i] == oStr[0] || s[i] == oStr[1] || s[i] == oStr[2]){
            //push(s[i])
            *++sp = s[i];
            stackSize++;
        }

        else if (s[i] == cStr[0] || s[i] == cStr[1] || s[i] == cStr[2]){

            if(s[i] == cStr[3]){
                if(*sp != oStr[3]){
                    return "NO";
                }
            }

            else if(s[i] == cStr[2]){
                if(*sp != oStr[2]){
                    return "NO";
                }  
            }

            else if(s[i] == cStr[1]){
                if(*sp != oStr[1]){
                    return "NO";
                }
            }

            if (stackSize > 0){
                --sp;
                stackSize--;
            }
            
        }

    }
    if (stackSize > 0){
        return "NO";
    }
    

    return "YES";

}


int main()
{
    FILE* fptr = fopen(getenv("OUTPUT_PATH"), "w");

    char* t_endptr;
    char* t_str = readline();
    int t = strtol(t_str, &t_endptr, 10);

    if (t_endptr == t_str || *t_endptr != '\0') { exit(EXIT_FAILURE); }

    for (int t_itr = 0; t_itr < t; t_itr++) {
        char* s = readline();

        char* result = isBalanced(s);

        fprintf(fptr, "%s\n", result);
    }

    fclose(fptr);

    return 0;
}

char* readline() {
    size_t alloc_length = 1024;
    size_t data_length = 0;
    char* data = malloc(alloc_length);

    while (true) {
        char* cursor = data + data_length;
        char* line = fgets(cursor, alloc_length - data_length, stdin);

        if (!line) { break; }

        data_length += strlen(cursor);

        if (data_length < alloc_length - 1 || data[data_length - 1] == '\n') { break; }

        size_t new_length = alloc_length << 1;
        data = realloc(data, new_length);

        if (!data) { break; }

        alloc_length = new_length;
    }

    if (data[data_length - 1] == '\n') {
        data[data_length - 1] = '\0';
    }

    data = realloc(data, data_length);

    return data;
}

